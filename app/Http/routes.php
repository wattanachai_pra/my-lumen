<?php

$app->get('/', function () use ($app) {
    return $app->version();
});

$app->get('encode', function (\Illuminate\Http\Request $request) {
  //echo $request->input('value');
    return response()->json([
        'result' => base64_encode($request->input('value')),
    ]);
});

$app->get('decode', function (\Illuminate\Http\Request $request) {
    return response()->json([
        'result' => base64_decode($request->input('value')),
    ]);
});
